package th.ac.tu.siit.calculator;



import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button b0 = (Button)findViewById(R.id.num0);
        b0.setOnClickListener(this);
        Button b1 = (Button)findViewById(R.id.num1);
        b1.setOnClickListener(this);
        Button b2 = (Button)findViewById(R.id.num2);
        b2.setOnClickListener(this);
        Button b3 = (Button)findViewById(R.id.num3);
        b3.setOnClickListener(this);
        Button b4 = (Button)findViewById(R.id.num4);
        b4.setOnClickListener(this);
        Button b5 = (Button)findViewById(R.id.num5);
        b5.setOnClickListener(this);
        Button b6 = (Button)findViewById(R.id.num6);
        b6.setOnClickListener(this);
        Button b7 = (Button)findViewById(R.id.num7);
        b7.setOnClickListener(this);
        Button b8 = (Button)findViewById(R.id.num8);
        b8.setOnClickListener(this);
        Button b9 = (Button)findViewById(R.id.num9);
        b9.setOnClickListener(this);
        
        ((Button)findViewById(R.id.add)).setOnClickListener(this);
        ((Button)findViewById(R.id.sub)).setOnClickListener(this);
        ((Button)findViewById(R.id.mul)).setOnClickListener(this);
        ((Button)findViewById(R.id.div)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.ac)).setOnClickListener(this);
        ((Button)findViewById(R.id.bs)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.dot)).setOnClickListener(this);
        ((Button)findViewById(R.id.equ)).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
   
    
    String display = "0";
    double last_number = 0;
    int last_sign = 0;
    
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		TextView output = (TextView)findViewById(R.id.output);
		
		switch (id)
		{
			case R.id.num0:
			case R.id.num1:
			case R.id.num2:
			case R.id.num3:
			case R.id.num4:
			case R.id.num5:
			case R.id.num6:
			case R.id.num7:
			case R.id.num8:
			case R.id.num9:
			case R.id.dot:
				if (display.equals("0"))
					display = "";
				
				if(id == R.id.dot)
				{
					if(!display.contains("."))
						display += ((Button)v).getText().toString();
				}
				else
				{
					display += ((Button)v).getText().toString();
				}
				
				output.setText(display);
			break;
			
			case R.id.bs:
				display  = display.substring(0, display.length()-1);
				if (display.equals(""))
					display = "0";
				output.setText(display);
			break;
			
			case R.id.ac:
				last_number = 0;
				last_sign = 0;
				display = "0";
				output.setText(display);
			break;
			
			case R.id.add:
				if (last_sign == 0)
				{
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 1;
					display = "0";
				}
				else
				{
					//////// EQU ////////
					double current_number = Double.parseDouble(output.getText().toString());
					if (last_sign == 1) {
						last_number = last_number + current_number;
					}
					if (last_sign == 2)
					{
						last_number = last_number - current_number;
					}
					if (last_sign == 3)
					{
						last_number = last_number * current_number;
					}
					if (last_sign == 4)
					{
						last_number = last_number / current_number;
					}
					output.setText(String.valueOf(last_number));
					//////// EQU ////////
					
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 1;
					
					display = "0";
				}
			break;
			
			case R.id.sub:
				if (last_sign == 0)
				{
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 2;
					display = "0";
				}
				else
				{
					//////// EQU ////////
					double current_number = Double.parseDouble(output.getText().toString());
					if (last_sign == 1) {
						last_number = last_number + current_number;
					}
					if (last_sign == 2)
					{
						last_number = last_number - current_number;
					}
					if (last_sign == 3)
					{
						last_number = last_number * current_number;
					}
					if (last_sign == 4)
					{
						last_number = last_number / current_number;
					}
					output.setText(String.valueOf(last_number));
					//////// EQU ////////
					
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 2;
					
					display = "0";
				}
			break;
			
			case R.id.mul:
				if (last_sign == 0)
				{
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 3;
					display = "0";
				}
				else
				{
					//////// EQU ////////
					double current_number = Double.parseDouble(output.getText().toString());
					if (last_sign == 1) {
						last_number = last_number + current_number;
					}
					if (last_sign == 2)
					{
						last_number = last_number - current_number;
					}
					if (last_sign == 3)
					{
						last_number = last_number * current_number;
					}
					if (last_sign == 4)
					{
						last_number = last_number / current_number;
					}
					output.setText(String.valueOf(last_number));
					//////// EQU ////////
					
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 3;
					
					display = "0";
				}
			break;
			
			case R.id.div:
				if (last_sign == 0)
				{
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 4;
					display = "0";
				}
				else
				{
					//////// EQU ////////
					double current_number = Double.parseDouble(output.getText().toString());
					if (last_sign == 1) {
						last_number = last_number + current_number;
					}
					if (last_sign == 2)
					{
						last_number = last_number - current_number;
					}
					if (last_sign == 3)
					{
						last_number = last_number * current_number;
					}
					if (last_sign == 4)
					{
						last_number = last_number / current_number;
					}
					output.setText(String.valueOf(last_number));
					//////// EQU ////////
					
					last_number = Double.parseDouble(output.getText().toString());
					last_sign = 4;
					
					display = "0";
				}
			break;
			
			case R.id.equ:			
				double current_number = Double.parseDouble(output.getText().toString());
				if (last_sign == 1) {
					last_number = last_number + current_number;
				}
				if (last_sign == 2)
				{
					last_number = last_number - current_number;
				}
				if (last_sign == 3)
				{
					last_number = last_number * current_number;
				}
				if (last_sign == 4)
				{
					last_number = last_number / current_number;
				}
				output.setText(String.valueOf(last_number));
				//////// EQU ////////
				
			
				last_sign = 0;
			break;
		}
		
		
	}
    
}

